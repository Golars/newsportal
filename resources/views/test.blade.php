@extends('layouts.base')

@section('title', $title)

@section('sidebar')
    @parent
    <p>This Is Post Data:</p>
    @foreach($data as $key => $value)
        <p>
            {{$key}} : {{$value}}
        </p>
    @endforeach
@endsection

@section('content')
    <p>This is my body content.</p>
    <form action="/post" method="POST">
        {{ csrf_field() }}
        <input type="text" name="login">
        <input type="text" name="password">
        <button type="submit">SEND</button>
    </form>
@endsection