<html>
    <head>
        <title>{{config('app.name')}} - @yield('title')</title>
    </head>
    <body>
    @section('sidebar')
        TITLE : {{config('app.name')}} - @yield('title') <br/>
        This is the master sidebar.
    @show

    <div class="container">
        @yield('content')
    </div>
    </body>
</html>