<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(){
        return view('test', [
            'title' => 'Test Page',
            'data' => []
        ]);
    }

    public function getPost(Request $request){
        return view('test', [
            'title' => 'Test Page',
            'data'  => $request->all()
        ]);
    }
}
